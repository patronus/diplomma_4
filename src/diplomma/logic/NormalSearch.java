package diplomma.logic;

import diplomma.model.Soft;

import java.sql.SQLException;

/**
 * User: Andrew.Nazymko
 */
public class NormalSearch implements Runnable {
    Soft required;

    public NormalSearch(Soft required) {
        this.required = required;
    }

    @Override
    public void run() {
        try {
            Engine engine = Engine.getEngine(required);
            engine.preCalculate();
//            Solution next = engine.next();
//            MainFace.setTableData(next.getItems());

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace(); //TODO:Work with it
        }
    }


}
