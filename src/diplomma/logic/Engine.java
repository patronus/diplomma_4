package diplomma.logic;

import diplomma.Logger;
import diplomma.data.Solution;
import diplomma.db.DBManager;
import diplomma.model.Soft;

import java.sql.SQLException;
import java.util.*;

/**
 * User: Andrew.Nazymko
 */
public class Engine {
    private static Engine engine;
    private Soft pattern;
    private ArrayList<Soft> allSoft;
    private ArrayList<Solution> solutions;
    private int position = 0;
    private Solution currentSolution;
    private Solution baseSolution;

    private Engine() {
        solutions = new ArrayList<>();
    }

    public static Engine getEngine(Soft pattern) throws SQLException {
        if (engine == null || !engine.pattern.equals(pattern)) {
            engine = new Engine();
            engine.setPattern(pattern);
            engine.setAllSoft((ArrayList<Soft>) DBManager.softList());
        }
        return engine;
    }

    public Soft getPattern() {
        return pattern;
    }

    public void setPattern(Soft pattern) {
        this.pattern = pattern;
    }

    public List<Soft> getAllSoft() {
        return allSoft;
    }

    public void setAllSoft(ArrayList<Soft> allSoft) {
        this.allSoft = allSoft;
    }

    public Solution next() {
        Solution solution;
        position++;
        if (position > solutions.size() - 1) {
            solution = findNext();
        } else {
            solution = solutions.get(position);
        }
        Logger.log("solution = " + solution);
        return solution;
    }

    public void preCalculate() throws CloneNotSupportedException {
        Soft pattern1 = getPattern();
        List<Soft> allSoft1 = getAllSoft();
        ArrayList<Solution> solutionArrayList = new ArrayList<>();
        resortByCoverQuality(allSoft1);

        System.out.println("pattern1 = " + Integer.toBinaryString(pattern1.requirement()));
        ArrayList<Solution> solutionArrayList1 = new ArrayList<>();
        ArrayList<Soft> removedList = new ArrayList<>();
        for (Soft soft : allSoft1) {
            Solution solution = new Solution();
            ArrayList<Soft> softs = new ArrayList<>(getAllSoft());
            removedList.add(soft);
            softs.removeAll(removedList);
            Solution solution1 = getSolution(solution, soft, softs);

            if (solution1 != null) {
                solutionArrayList1.add(solution1);
            }
        }
        System.out.println("solutionArrayList1 = " + solutionArrayList1);
    }

    private Solution getSolution(Solution solution, Soft soft, ArrayList<Soft> softs) throws CloneNotSupportedException {
        int itemCoverQuality = solution.getQuality() | getPattern().requirement();
        System.out.println("pattern = " + Integer.toBinaryString(pattern.requirement()) + ", itemCoverQuality = " + Integer.toBinaryString(itemCoverQuality));
        if (itemCoverQuality == Soft.getFull()) {
            softs.remove(soft);
            System.out.println("solution = " + solution);
            return solution;
        } else {
            if (softs.size() > 0) {
                softs = removeWorst(softs, soft);
                Soft bestSoft = getBestSoft(softs);
                solution.addSoft(bestSoft);
                return solution = getSolution(solution.clone(), bestSoft, softs);
            } else {
                return null;
            }
        }
    }

    private void resortByCoverQuality(List<Soft> allSoft1) {
        Collections.sort(allSoft1, new Comparator<Soft>() {
            @Override
            public int compare(Soft o1, Soft o2) {
                int val1 = getPattern().requirement() | o1.getValue();
                int val2 = getPattern().requirement() | o2.getValue();
                if (val1 > val2) {
                    return 1;
                } else if (val1 < val2) {
                    return -1;
                }
                return 0;
            }
        });
    }

    private Soft getBestSoft(ArrayList<Soft> softList) {
        resortByCoverQuality(softList);
        return softList.remove(0);
    }

    private ArrayList<Soft> removeWorst(List<Soft> allItemList, Soft bestItem) {
        Iterator<Soft> iterator = allItemList.iterator();
        ArrayList<Soft> newSoft = new ArrayList<>();
        int bestState = getPattern().requirement() & bestItem.getValue();
        while (iterator.hasNext()) {
            Soft next = iterator.next();
            int itemState = getPattern().requirement() | next.getValue();
            if ((bestState | itemState) != bestState) {
                newSoft.add(next);
            }
        }
        return newSoft;
    }

    private Solution findNext() {
        if (solutions.size() > 0) {
            try {
                Solution solution = solutions.get(solutions.size() - 1).clone();
                List<Soft> tmpSoftList = allSoft.subList(0, allSoft.size() - 1);
                solution.putDropped(solution.removeLast());
                //Soft or already in solution, or was denied
                tmpSoftList.removeAll(solution.getDroppedSoft());
                tmpSoftList.removeAll(solution.getItems());

                while (!tmpSoftList.isEmpty()) {
                    Soft candidate = tmpSoftList.remove(0);

                }


            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        solutions.add(new Solution());
        return solutions.get(solutions.size() - 1);
    }

    public Solution prev() {
        if (position > 0) {
            position--;
        }
        return solutions.get(position);
    }


}
