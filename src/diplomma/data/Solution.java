package diplomma.data;

import diplomma.model.Soft;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Andrew.Nazymko
 */
public class Solution implements Cloneable {
    ArrayList<Soft> droppedSolutions;
    ArrayList<Soft> items;
    int features;
    private Integer quality=0;

    public Solution() {
        items = new ArrayList<>();
        droppedSolutions = new ArrayList<>();
    }

    @Override
    public Solution clone() throws CloneNotSupportedException {
        Solution solution = new Solution();
        solution.addSoft(getItems());
        return solution;
    }

    public ArrayList<Soft> getItems() {
        return items;
    }

    public int getFeatures() {
        return features;
    }

    public void addSoft(Soft soft) {
        features += soft.getFeatures();
        quality = quality | soft.getValue();
        items.add(soft);
    }

    public void addSoft(List<Soft> softs) {
        for (Soft soft : softs) {
            addSoft(soft);
        }
    }

    @Override
    public String toString() {
        return "Solution{" +
                "items=" + items +
                ", features=" + features +
                '}';
    }

    public Soft removeLast() {
        if (getItems().size() > 0) {
            return getItems().remove(getItems().size());
        }
        return null;
    }

    public ArrayList<Soft> getDroppedSoft() {
        return droppedSolutions;
    }

    public void putDropped(Soft soft) {
        getDroppedSoft().add(soft);
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }
}
