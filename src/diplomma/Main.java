package diplomma;

import diplomma.db.DBCreator;
import diplomma.db.DBManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {


    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String RELIABILITY = "realibility";
    public static final String ENCRIPTION = "encription";
    public static final String NSD = "NSD";
    public static final String NSK = "NSK";
    public static final String OBFUSKATION = "obfuskation";
    public static final String ANTIVIRUS = "antivirus";
    public static final String PACK = "pack";
    public static final String MONITORING = "monitoring";
    public static final String COMPRESSION = "compression";
    public static final String PASSWORD = "password";
    public static final String UPGRADE = "upgrade";
    public static final String COMFORT = "comfort";
    public static final String ID = "id";
    public static final String FEATURES = "features";
    public static List<String> fields = new ArrayList<String>() {{
        add(NAME);
        add(PRICE);
        add(RELIABILITY);

        add(ENCRIPTION);
        add(NSD);
        add(NSK);
        add(OBFUSKATION);
        add(ANTIVIRUS);
        add(PACK);
        add(MONITORING);
        add(COMPRESSION);
        add(PASSWORD);
        add(UPGRADE);
        add(COMFORT);
        add(FEATURES);

    }};

    public static List<String> getFields() {
        return fields;
    }

    public static void main(String[] args) throws SQLException {
        if (args.length > 0) {
            String arg = args[0];
            Logger.DEBUG = arg.equals("true");
        }
        DBCreator creator = new DBCreator();
        createSoftTable(creator);

        launch(args);
    }

    private static void createSoftTable(DBCreator creator) {
        ArrayList<Pair> pairs = new ArrayList<Pair>();
        {
            pairs.add(new Pair(NAME, "string"));
            pairs.add(new Pair(PRICE, "real"));
            pairs.add(new Pair(RELIABILITY, "real"));

            pairs.add(new Pair(ENCRIPTION, "boolean default true"));
            pairs.add(new Pair(NSD, "boolean default true"));
            pairs.add(new Pair(NSK, "boolean default true"));
            pairs.add(new Pair(OBFUSKATION, "boolean default true"));
            pairs.add(new Pair(ANTIVIRUS, "boolean default true"));
            pairs.add(new Pair(PACK, "boolean default true"));
            pairs.add(new Pair(MONITORING, "boolean default true"));
            pairs.add(new Pair(COMPRESSION, "boolean default true"));
            pairs.add(new Pair(PASSWORD, "boolean default true"));
            pairs.add(new Pair(UPGRADE, "boolean default true"));
            pairs.add(new Pair(COMFORT, "boolean default true"));
        }
        try {
            creator.create(DBManager.getConnect(), "soft", true, pairs);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        stage.setTitle("Diploma");
        stage.setScene(new Scene(root, 700, 500));
        stage.show();
    }
}
