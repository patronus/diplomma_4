package diplomma.controllers;

import diplomma.Logger;
import diplomma.Main;
import diplomma.db.DBManager;
import diplomma.logic.SimpleSearch;
import diplomma.model.Soft;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by: Andrew on: 9/24/13.
 */
public class MainFace {
    private static final ObservableList<Soft> data =
            FXCollections.observableArrayList();
    public TableView table;
    //    public CheckBox reliability;
    public CheckBox encription;
    public CheckBox nsd;
    public CheckBox nsk;
    public CheckBox obfuscation;
    public CheckBox monitoring;
    public CheckBox pack;
    public CheckBox antivirus;
    public CheckBox comfort;
    public CheckBox upgrade;
    public CheckBox compression;
    public CheckBox password;
    public TextField max_price;
    public Tab editPanel;
    private boolean columnsReady =false;

    public static ObservableList<Soft> getData() {
        return data;
    }

    public static void setTableData(List<Soft> items) {
        data.clear();
        data.addAll(items);
        if (items.size() == 0) {
            Logger.log("No results found");
        }
    }

    public void onCreate() {

    }

    public void searchAction(ActionEvent actionEvent) {
        final Soft soft = new Soft();
        {
            soft.setEncription(encription.isSelected());
            soft.setNSD(nsd.isSelected());
            soft.setNSK(nsk.isSelected());
            soft.setObfuskation(obfuscation.isSelected());
            soft.setMonitoring(monitoring.isSelected());
            soft.setPack(pack.isSelected());
            soft.setAntivirus(antivirus.isSelected());
            soft.setComfort(comfort.isSelected());
            soft.setUpgrade(upgrade.isSelected());
            soft.setCompression(compression.isSelected());
            soft.setPassword(password.isSelected());
        }
        //Simple search
        new Thread(new SimpleSearch(soft)).start();

        Logger.log("soft = " + soft);
        Logger.log("soft(nor) = " + Integer.toBinaryString(soft.getValue()));
        Logger.log("soft(rev) = " + Integer.toBinaryString(soft.requirement()));
        Logger.log("soft(max) = " + Integer.toBinaryString(soft.getMax()));
    }

    public void onViewOpen(Event event) throws SQLException {
        boolean selected = ((Tab) event.getTarget()).isSelected();
        if (!columnsReady) {
            List<String> fields = Main.getFields();
            for (String field : fields) {
                TableColumn<Soft, String> col = new TableColumn<>(field);
                col.setCellValueFactory(new PropertyValueFactory<Soft, String>(field));
                table.getColumns().add(col);
            }
            columnsReady = true;
            table.setItems(data);
        }
        if (selected) {
            Logger.log("View open");
            List<Soft> softs = DBManager.softList();
            for (Soft soft : softs) {
                Logger.log("soft = " + soft);
                if (!data.contains(soft)) {
                    data.add(soft);
                }
                table.setItems(data);
            }


        }


    }

    public void onEditOpen(Event event) {
        boolean selected = ((Tab) event.getTarget()).isSelected();
        if (selected) {
            Logger.log("Edit open");
        }
    }
}
