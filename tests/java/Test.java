package java;

import diplomma.Logger;
import diplomma.db.DBManager;
import diplomma.model.Soft;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrew on 9/24/13.
 */
public class Test {
    public static void main(String[] args) throws SQLException {
        Logger.log(0b00001111 | 0b11110000);
        Logger.log(2 << 1);


        List<Soft> softs = DBManager.softList();
        for (Soft soft : softs) {
            Logger.log("soft = " + soft);
            Logger.log("value = " + soft.getValue());
        }

    }
}
