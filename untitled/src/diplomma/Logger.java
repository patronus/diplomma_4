package diplomma;

/**
 * User: Andrew.Nazymko
 */
public class Logger {
    public static boolean DEBUG = false;

    public static void log(Object o) {
        if (DEBUG) {
            System.out.println("LOG:" + o);
        }
    }
}
