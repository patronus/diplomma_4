package diplomma.controllers;

import diplomma.Logger;
import diplomma.Main;
import diplomma.data.Solution;
import diplomma.db.DBManager;
import diplomma.logic.Engine;
import diplomma.logic.NormalSearch;
import diplomma.model.Soft;
import diplomma.model.Stats;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by: Andrew on: 9/24/13.
 */
public class MainFace {
    private static final ObservableList<Soft> data =
            FXCollections.observableArrayList();
    private static MainFace _this;
    public TableView table;
    public TableView edit_table;
    //    public CheckBox reliability;
    public CheckBox encription;
    public CheckBox nsd;
    public CheckBox nsk;
    public CheckBox obfuscation;
    public CheckBox monitoring;
    public CheckBox pack;
    public CheckBox antivirus;
    public CheckBox comfort;
    public CheckBox upgrade;
    public CheckBox compression;
    public CheckBox password;
    public TextField max_price;
    public Tab editPanel;
    public Button next;
    public Button prev;
    public Text found;
    public Text fail;
    public ChoiceBox os_box;
    public CheckBox spyware;

    private boolean columnsReady = false;

    private Soft soft;

    private boolean editColumnsReady = false;

    public MainFace() {
        _this = this;

    }

    public static ObservableList<Soft> getData() {
        return data;
    }

    public static MainFace getSelf() {
        return _this;

    }

    public static void setTableData(List<Soft> items) {
        data.clear();
        data.addAll(items);
        if (items.size() == 0) {
            Logger.log("No results found");
        }
    }

    public void setStats(Stats stats) {
        found.setText(String.valueOf(stats.getFound()));
        fail.setText(String.valueOf(stats.getFail()));
    }


    public void searchAction(ActionEvent actionEvent) {
        soft = new Soft();
        {
            soft.setAntivirus(antivirus.isSelected());
            soft.setNSK(nsk.isSelected());
            soft.setNSD(nsd.isSelected());
            soft.setMonitoring(monitoring.isSelected());
            soft.setSpyware(spyware.isSelected());
            soft.setUpgrade(upgrade.isSelected());
            try {
                soft.setPrice(Double.valueOf(max_price.getText()));
            } catch (Exception e) {
                //No value. Let it fail
            }
        }
        new Thread(new NormalSearch(soft)).start();
    }

    public void onViewOpen(Event event) throws SQLException {
        boolean selected = ((Tab) event.getTarget()).isSelected();

        if (!columnsReady) {
            Logger.log("columnsReady");
            List<String> fields = Main.getFields();
            for (String field : fields) {
                TableColumn<Soft, String> col = new TableColumn<>(field);
                col.setCellValueFactory(new PropertyValueFactory<Soft, String>(field));
                table.getColumns().add(col);
            }
            columnsReady = true;
            table.setItems(data);
        }
        if (selected) {
            Logger.log("View open");
            List<Soft> softs = DBManager.softList();
            for (Soft soft : softs) {
                Logger.log("soft = " + soft);
                if (!data.contains(soft)) {
                    data.add(soft);
                }
                table.setItems(data);
            }
        }
    }

    public void onEditOpen(Event event) throws SQLException {
        boolean selected = ((Tab) event.getTarget()).isSelected();
        if (!editColumnsReady) {
            Logger.log("Edit opened");
            List<String> fields = Main.getFields();
            for (String field : fields) {
                TableColumn<Soft, String> col = new TableColumn<>(field);
                col.setCellValueFactory(new PropertyValueFactory<Soft, String>(field));
                col.setEditable(true);
                edit_table.getColumns().add(col);
            }
            editColumnsReady = true;
            edit_table.setItems(data);
        }
        if (selected) {
            Logger.log("View open");
            List<Soft> softs = DBManager.softList();
            for (Soft soft : softs) {
                Logger.log("soft = " + soft);
                if (!data.contains(soft)) {
                    data.add(soft);
                }
                edit_table.setItems(data);
                edit_table.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        if (mouseEvent.getClickCount() > 1) {
                            TablePosition selectedCells = (TablePosition) edit_table.getSelectionModel().getSelectedCells().get(0);
                            int row = selectedCells.getRow();
                            Soft soft1 = data.get(row);
                            System.out.println("CLICK! " + soft1.getName());
                            openNewFace(soft1.getId());
                        }
                    }
                });
            }
        }

    }

    public void showPrev(ActionEvent actionEvent) throws SQLException {
        Solution prev = Engine.getEngine(soft).prev();
        MainFace.setTableData(prev.getItems());
    }

    public void ShowNext(ActionEvent actionEvent) throws SQLException, CloneNotSupportedException {
        Engine engine = Engine.getEngine(soft);
        Solution next = engine.next();
        Stats stats = engine.getStats();

        MainFace.setTableData(next.getItems());
        MainFace.getSelf().setStats(stats);

    }

    public void openNewFace(int itemID) {

    }

}
