package diplomma.model;

/**
 * User: Andrew
 */
public class Stats {
    int fail=0;
    int found=0;

    public int getFail() {
        return fail;
    }

    public void setFail(int fail) {
        this.fail = fail;
    }

    public int getFound() {
        return found;
    }

    public void setFound(int found) {
        this.found = found;
    }

    public Stats(int fail, int found) {

        this.fail = fail;
        this.found = found;
    }
}
