package diplomma.model;

/**
 * User: Andrew.Nazymko
 */
public class Soft implements Comparable {
    static int full = (int) 0b11111111111;
    Integer requirement;
    private Integer id = null;
    private String name;
    private double price;
    private boolean antivirus;
    private boolean NSK;
    private boolean NSD;
    private boolean monitoring;
    private boolean spyware;

    private boolean upgrade;
    private double comfort;
    private boolean windows;
    private boolean linux;
    private double effective;

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isSpyware() {
        return spyware;
    }

    public void setSpyware(boolean spyware) {
        this.spyware = spyware;
    }

    public boolean isWindows() {
        return windows;
    }

    public void setWindows(boolean windows) {
        this.windows = windows;
    }

    public boolean isLinux() {
        return linux;
    }

    public void setLinux(boolean linux) {
        this.linux = linux;
    }

    public double getEffective() {
        return effective;
    }

    public void setEffective(double effective) {
        this.effective = effective;
    }

    public void setFeatures(int features) {
        this.features = features;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    private int features;
    private Integer value = 0;

    public static int getFull() {
        return full;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Soft soft = (Soft) o;

        if (id != null ? !id.equals(soft.id) : soft.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Soft{" +
                "requirement=" + requirement +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", antivirus=" + antivirus +
                ", NSK=" + NSK +
                ", NSD=" + NSD +
                ", monitoring=" + monitoring +
                ", spyware=" + spyware +
                ", upgrade=" + upgrade +
                ", comfort=" + comfort +
                ", windows=" + windows +
                ", linux=" + linux +
                ", effective=" + effective +
                ", features=" + features +
                ", value=" + value +
                '}';
    }


    public Integer getValue() {
        if (value == null) {
            value = new Integer(0);

            increase(antivirus, 0);
            increase(NSK, 1);
            increase(NSD, 2);
            increase(monitoring, 3);
            increase(spyware, 4);
            increase(upgrade, 5);


        }

        return value;

    }

    public Integer requirement() {
        if (requirement == null) {
            requirement = getMax() ^ getValue();
        }
        return requirement;
    }

    private void increase(boolean required, int pow) {
        if (required) {
            value += 2 << pow;
            features++;
        }
    }


    public int getMax() {
        return full;
    }

    public Integer getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAntivirus() {
        return antivirus;
    }

    public void setAntivirus(boolean antivirus) {
        this.antivirus = antivirus;
    }


    public boolean isNSK() {
        return NSK;
    }

    public void setNSK(boolean NSK) {
        this.NSK = NSK;
    }

    public boolean isNSD() {
        return NSD;
    }

    public void setNSD(boolean NSD) {
        this.NSD = NSD;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }


    public boolean isUpgrade() {
        return upgrade;
    }

    public void setUpgrade(boolean upgrade) {
        this.upgrade = upgrade;
    }

    public double getComfort() {
        return comfort;
    }

    public void setComfort(double comfort) {
        this.comfort = comfort;
    }

    public int getFeatures() {
        return features;
    }

    @Override
    public int compareTo(Object o) {

        if (o instanceof Soft) {
            Soft s = (Soft) o;
            if (s.getFeatures() < getFeatures()) {
                return -1;
            } else if (s.getFeatures() > getFeatures()) {
                return 1;
            } else {
                return 0;
            }
        }
        return -1;
    }
}
