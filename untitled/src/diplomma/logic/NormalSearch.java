package diplomma.logic;

import diplomma.controllers.MainFace;
import diplomma.data.Solution;
import diplomma.model.Soft;
import diplomma.model.Stats;

import java.sql.SQLException;

/**
 * User: Andrew.Nazymko
 */
public class NormalSearch implements Runnable {
    Soft required;

    public NormalSearch(Soft required) {
        this.required = required;
    }

    @Override
    public void run() {
        try {
            Engine engine = Engine.getEngine(required);

            Solution next = engine.next();
//            System.out.println("next = " + next);
            if (next != null) {
                MainFace.setTableData(next.getItems());
                Stats stats= Engine.getEngine(required).getStats();
                MainFace.getSelf(). setStats(stats);

            } else {
                System.out.println("Nothing found more");
            }
        } catch (SQLException | CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }


}
