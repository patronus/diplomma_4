package diplomma.logic;

import diplomma.Logger;
import diplomma.data.Solution;
import diplomma.db.DBManager;
import diplomma.model.Soft;
import diplomma.model.Stats;

import java.sql.SQLException;
import java.util.*;

/**
 * User: Andrew.Nazymko
 */
public class Engine {
    private static Engine engine;
    private Soft pattern;
    private ArrayList<Soft> allSoft;
    private ArrayList<Solution> solutions;
    private ArrayList<Solution> failSolutions;
    private int position = 0;

    boolean state = false;
    private int failVariantsCounter = 0;

    public boolean isEnd() {
        return state;
    }

    private Engine() {
        solutions = new ArrayList<>();
        failSolutions = new ArrayList<>();
    }

    public int getFailVariantsCounter() {
        return failVariantsCounter;
    }

    public static Engine getEngine(Soft pattern) throws SQLException {
        if (engine == null || !engine.pattern.equals(pattern)) {
            engine = new Engine();
            engine.setPattern(pattern);
            engine.setAllSoft((ArrayList<Soft>) DBManager.softList());

        }
        return engine;
    }

    public Soft getPattern() {
        return pattern;
    }

    public void setPattern(Soft pattern) {
        this.pattern = pattern;
    }

    public ArrayList<Soft> getAllSoft() {
        return allSoft;
    }

    public void setAllSoft(ArrayList<Soft> allSoft) {
        this.allSoft = allSoft;
    }

    public Solution next() throws CloneNotSupportedException {
        if (position > solutionsSize() - 1) {
//            preCalculate();
            return findNext();
        } else {
            Solution solution = getSolution(position);
            position++;
            return solution;

        }
    }

//    public void preCalculate() throws CloneNotSupportedException {
//        Soft pattern1 = getPattern();
//        List<Soft> allSoft1 = getAllSoft();
//        ArrayList<Solution> solutionArrayList = new ArrayList<>();
//        resortByCoverQuality(allSoft1);
//
////        System.out.println("pattern1 = " + Integer.toBinaryString(pattern1.requirement()));
//        ArrayList<Soft> removedList = new ArrayList<>();
//        for (Soft soft : allSoft1) {
//            Solution solution = new Solution();
//            ArrayList<Soft> softs = new ArrayList<>(getAllSoft());
//            removedList.add(soft);
//            softs.removeAll(removedList);
//            Solution solutionVariant = getSolution(solution, soft, softs);
//
//            if (solutionVariant != null && solutionVariant.getItems().size() > 0 && solutionVariant.getFeatures() > 0) {
//                verify(solutionVariant);
//                Collections.sort(getSolutions(), new Comparator<Solution>() {
//                    @Override
//                    public int compare(Solution item1, Solution item2) {
//                        return item1.getItems().size() < item2.getItems().size() ? -1 : item1.getItems().size() > item2.getItems().size() ? 1 : 0;
//                    }
//                });
////                System.out.println("solutionVariant added= " + solutionVariant);
//            }
//        }
//
//    }

//    private Solution getSolution(Solution solution, Soft soft, ArrayList<Soft> softs) throws CloneNotSupportedException {
//        int itemCoverQuality = solution.getQuality() | getPattern().requirement();
////        System.out.println("pattern = " + Integer.toBinaryString(pattern.requirement()) + ", itemCoverQuality = " + Integer.toBinaryString(itemCoverQuality));
//        if (itemCoverQuality == Soft.getFull()) {
//            softs.remove(soft);
////            System.out.println("solution = " + solution);
//            return solution;
//        } else {
//            if (softs.size() > 0) {
//                softs = removeWorst(softs, soft);
//                Soft bestSoft = getBestSoft(softs);
//                solution.addSoft(bestSoft);
//                return getSolution(solution.clone(), bestSoft, softs);
//            } else {
//                return null;
//            }
//        }
//    }

    private void resortByCoverQuality(List<Soft> allSoft1) {
        Collections.sort(allSoft1, new Comparator<Soft>() {
            @Override
            public int compare(Soft o1, Soft o2) {
                int val1 = getPattern().requirement() | o1.getValue();
                int val2 = getPattern().requirement() | o2.getValue();
                if (val1 > val2) {
                    return 1;
                } else if (val1 < val2) {
                    return -1;
                }
                return 0;
            }
        });
    }

//    private Soft getBestSoft(ArrayList<Soft> softList) {
//        resortByCoverQuality(softList);
//        return softList.remove(0);
//    }

    private ArrayList<Soft> removeWorst(List<Soft> allItemList, Soft bestItem) {
        Iterator<Soft> iterator = allItemList.iterator();
        ArrayList<Soft> newSoft = new ArrayList<>();
        int bestState = getPattern().requirement() & bestItem.getValue();
        while (iterator.hasNext()) {
            Soft next = iterator.next();
            int itemState = getPattern().requirement() | next.getValue();
            if ((bestState | itemState) != bestState) {
                newSoft.add(next);
            }
        }
        return newSoft;
    }

    public ArrayList<Solution> getSolutions() {
        return solutions;
    }

    private Solution findNext() {
        System.out.println("Find next");
        if (failSolutions.size() > 0) {
            try {
                if (position < solutionsSize()) {
                    Solution solution = getSolution(position);
                    position++;
                    return solution;
                }
                boolean isSuccesfullSearch = false;
                for (Soft candidate : getAllSoft()) {
                    while (!failSolutions.isEmpty()) {
                        Solution failSolution = failSolutions.remove(0);
                        if (!failSolution.getItems().contains(candidate)) {
                            Solution clone = failSolution.clone();
                            clone.addSoft(candidate);
                            verify(clone);
                            isSuccesfullSearch = true;
                        }
                    }
                }
                if (!isSuccesfullSearch) {
                    System.out.println("I return null(" + failSolutions.size() + ", " + solutions.size() + ")");
                    return null;
                    //Well done! we search all what we can
                }
                findNext();

            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        } else {
            for (final Soft soft : getAllSoft()) {
                if ((soft.getValue() | pattern.getValue()) != Soft.getFull()) {
                    failSolutions.add(new Solution() {{
                        addSoft(soft);
                        failVariantsCounter++;
                    }});
                } else {
                    verify(new Solution() {{
                        addSoft(soft);
                    }});
                }
            }
            return findNext();
        }
        return getSolution(solutionsSize() - 1);
    }

    private void verify(Solution clone) {
        if ((clone.getQuality() | pattern.requirement()) == Soft.getFull()) {
            if (pattern.getPrice() > 0) {
                if (!(clone.getPrice() > pattern.getPrice())) {
                    addSolution(clone);
//                    System.out.println("Added(price cap):" + clone);
                } else {
                    //Drop it.

                    failVariantsCounter++;
                }
            } else {
//                System.out.println("Added(no price cap):" + clone);
                addSolution(clone);

            }
        }
    }

    public Solution prev() {
        if (position > 0) {
            position--;
        }
        return getSolution(position);
    }


    protected void addSolution(Solution s) {
        if (!getSolutions().contains(s))
            getSolutions().add(s);
    }

    protected Solution getSolution(int i) {
        return getSolutions().get(i);

    }

    protected int solutionsSize() {
        return getSolutions().size();
    }

    public Stats getStats() {
        return new Stats(failVariantsCounter, solutions.size());
    }
}

