package diplomma.db;

import diplomma.Logger;
import diplomma.Main;
import diplomma.model.Soft;
import org.sqlite.JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * User: Andrew.Nazymko
 */
public class DBManager {
    public static final String DB_CONNECT = "jdbc:sqlite:mountain.sqlite";
    private static final JDBC jdbc;
    private static Connection connect;

    static {
        jdbc = new JDBC();
        try {
            connect = jdbc.connect(DB_CONNECT, new Properties());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnect() {
        return connect;
    }

    public static List<Soft> softList() throws SQLException {
        Connection connect1 = getConnect();
        ResultSet resultSet = connect1.createStatement().executeQuery("SELECT  * FROM  soft");
        ArrayList<Soft> items = new ArrayList<>();

        while (resultSet.next()) {
            Soft s = new Soft();

            int id = resultSet.getInt("id");

            String name = resultSet.getString(Main.NAME);
            double price = resultSet.getDouble(Main.PRICE);
            boolean antivirus = resultSet.getBoolean(Main.ANTIVIRUS);
            boolean nsk = resultSet.getBoolean(Main.NSK);
            boolean nsd = resultSet.getBoolean(Main.NSD);
            boolean monitoring = resultSet.getBoolean(Main.MONITORING);
            boolean spyware = resultSet.getBoolean(Main.SPYWARE);
            boolean upgrade = resultSet.getBoolean(Main.UPGRADE);
            double comfort = resultSet.getDouble(Main.COMFORT);
            boolean windows = resultSet.getBoolean(Main.WINDOWS);
            boolean linux = resultSet.getBoolean(Main.LINUX);
            double effective = resultSet.getDouble(Main.EFFECTIVE);


            s.setId(id);
            s.setName(name);
            s.setPrice(price);
            s.setAntivirus(antivirus);
            s.setNSK(nsk);
            s.setNSD(nsd);
            s.setMonitoring(monitoring);
            s.setComfort(comfort);
            s.setUpgrade(upgrade);
            s.setWindows(windows);
            s.setLinux(linux);
            s.setEffective(effective);

            //Calculate metadata
            s.getValue();

            items.add(s);
        }

        Collections.sort(items);
        return items;
    }

    public static boolean save(Soft item) {
        try {
            if (item.getId() != null) {
                update(item);
                return true;
            } else {
                save_0(item);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private static void update(Soft item) throws SQLException {

        ResultSet resultSet = getConnect().createStatement().executeQuery("select * from soft where id=" + item.getId());
        resultSet.next();
        {

            resultSet.updateString(Main.NAME, item.getName());
            resultSet.updateDouble(Main.PRICE, item.getPrice());
            resultSet.updateBoolean(Main.ANTIVIRUS, item.isAntivirus());
            resultSet.updateBoolean(Main.NSK, item.isNSK());
            resultSet.updateBoolean(Main.NSD, item.isNSD());
            resultSet.updateBoolean(Main.MONITORING, item.isMonitoring());
            resultSet.updateBoolean(Main.SPYWARE, item.isSpyware());
            resultSet.updateBoolean(Main.UPGRADE, item.isUpgrade());
            resultSet.updateDouble(Main.COMFORT, item.getComfort());
            resultSet.updateBoolean(Main.WINDOWS, item.isWindows());
            resultSet.updateBoolean(Main.LINUX, item.isLinux());
            resultSet.updateDouble(Main.EFFECTIVE, item.getEffective());
        }

    }

    private static void save_0(Soft item) {
        Connection connect1 = getConnect();
        String sql = insertSoft(item);
        Logger.log("sql = " + sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connect1.prepareStatement(sql);
            boolean execute = preparedStatement.execute();
            preparedStatement.close();
            Logger.log("execute = " + execute);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static String insertSoft(Soft item) {
        StringBuilder builder = new StringBuilder();
        builder.append("insert into soft ").append(getSoftColumnList()).append(" values ").append(prepareValues(item));

        return builder.toString();  //To change body of created methods use File | Settings | File Templates.
    }

    private static String prepareValues(Soft item) {
        StringBuilder vBuilder = new StringBuilder();
        vBuilder.append("(");
        {
            vBuilder.append("'").append(item.getName()).append("'").append(", ");
            vBuilder.append(item.getPrice()).append(", ");

            vBuilder.append(toI(item.isAntivirus())).append(", ");
            vBuilder.append(toI(item.isNSK())).append(", ");
            vBuilder.append(toI(item.isNSD())).append(", ");
            vBuilder.append(toI(item.isMonitoring())).append(", ");
            vBuilder.append(toI(item.isSpyware())).append(", ");
            vBuilder.append(toI(item.isUpgrade())).append(", ");
            vBuilder.append(item.getComfort()).append(", ");
            vBuilder.append(toI(item.isWindows())).append(", ");
            vBuilder.append(toI(item.isLinux())).append(", ");
            vBuilder.append(item.getEffective());


        }

        vBuilder.append(")");
        return vBuilder.toString();

    }

    public static void resetConnection() throws SQLException {
        getConnect().close();
        connect = jdbc.connect(DB_CONNECT, new Properties());
    }

    public static String getSoftColumnList() {
        StringBuilder colBuilder = new StringBuilder();
        colBuilder.append("(");
        List<String> fields = Main.getFields();
        Iterator<String> iterator = fields.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (iterator.hasNext()) {
                colBuilder.append(next).append(", ");
            } else {
                colBuilder.append(next);
            }
        }
        colBuilder.append(")");

        return colBuilder.toString();
    }

    //SQLite require 1 or 0 for boolean values
//    private int magicTransferBoolToInt()
    private static int toI(boolean value) {
        if (value)
            return 1;
        else
            return 0;

    }
}
