package diplomma.db;

import diplomma.Logger;
import diplomma.Params;
import diplomma.model.Soft;
import javafx.util.Pair;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: Andrew.Nazymko
 */
public class DBCreator {

    public static final String MOCK_DATA_TXT = "mock_data.txt";

    public void create(Connection con, String tableName, boolean id) throws SQLException, IOException {


        ResultSet resultSet = con.createStatement().executeQuery("SELECT * FROM sqlite_master WHERE type='table'");
        boolean exist = isDbExist(tableName, resultSet);
        if (!exist) {

            createSoftTable(con, tableName, id);
            DBManager.resetConnection();
            dbSeed(MOCK_DATA_TXT);
        }

    }

    public boolean isDbExist(String tableName, ResultSet resultSet) throws SQLException {
        boolean exist = false;
        while (resultSet.next()) {
            String name = resultSet.getString("name");
            if (tableName.equals(name)) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    private void createSoftTable(Connection con, String tableName, boolean id) throws SQLException, IOException {
        List<Params> p = readColumns(MOCK_DATA_TXT);
        List<Pair> pairs = new ArrayList<>();
        for (Params params : p) {
            pairs.add(new Pair(params.getName(), params.getType()));
        }
        String sql = getSql(tableName, id, pairs);
        Logger.log("sql = " + sql);
        Statement stmt = con.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();

        con.close();
        Logger.log("Table '" + tableName + "' created");
    }

    public String getSql(String tableName, boolean id, List<Pair> fields) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(tableName).append(" (");
        if (id) {
            builder.append("id INTEGER PRIMARY KEY").append(", ");
        }
        Iterator<Pair> iterator = fields.iterator();
        while (iterator.hasNext()) {
            Pair next = iterator.next();
            if (iterator.hasNext()) {
                builder.append(next.getKey()).append(" ").append(next.getValue()).append(", ");
            } else {
                builder.append(next.getKey()).append(" ").append(next.getValue()).append(")");
            }
        }

        return builder.toString();
    }

    public void dbSeed(String file) throws IOException {
        File f = new File(file);

        FileReader fReader = new FileReader(f);
        BufferedReader bReader = new BufferedReader(fReader);
        String s;
        String columnNames = bReader.readLine();
        List<Params> cols = parseColumn(columnNames);

        createSoftTable(cols);

        while ((s = bReader.readLine()) != null) {
            Soft soft = parseSoft(s);
            DBManager.save(soft);
        }


    }

    public ArrayList<Params> readColumns(String file) throws IOException {
        File f = new File(file);
        FileReader fReader = new FileReader(f);
        BufferedReader bReader = new BufferedReader(fReader);
        String columnNames = bReader.readLine();
        List<Params> cols = parseColumn(columnNames);
        return (ArrayList<Params>) cols;
    }

    private List<Params> parseColumn(String columnNames) {

        String[] split = columnNames.trim().split("\\|");
        System.out.println("columnNames = " + columnNames);
        ArrayList<Params> parameters = new ArrayList<>();
        for (String element : split) {
            String[] params = element.split("\\.");
            System.out.println("element = " + element);
            parameters.add(new Params(params[0], params[1]));
        }

        return parameters;  //To change body of created methods use File | Settings | File Templates.
    }

    private Soft parseSoft(String s) {
        Soft soft = new Soft();

        String[] split = s.split("\\|");
        for (int pos = 0; pos < split.length; pos++) {
            String value = split[pos].trim();
            switch (pos) {
                case 0:
                    soft.setName(value);
                    break;
                case 1:
                    soft.setPrice(Double.parseDouble(value));
                    break;
                case 2:
                    soft.setAntivirus(parseBoolean(value));
                    break;
                case 3:
                    soft.setNSK(parseBoolean(value));
                    break;
                case 4:
                    soft.setNSD(parseBoolean(value));
                    break;
                case 5:
                    soft.setMonitoring(parseBoolean(value));

                    break;
                case 6:
                    soft.setSpyware(parseBoolean(value));
                    break;
                case 7:
                    soft.setUpgrade(parseBoolean(value));
                    break;
                case 8:
                    soft.setComfort(Double.valueOf(value));
                    break;
                case 9:
                    soft.setWindows(parseBoolean(value));
                    break;
                case 10:
                    soft.setLinux(parseBoolean(value));
                    break;
                case 11:
                    soft.setEffective(Double.valueOf(value));
                    break;
                default:
                    //GTFO , Indus code style!
                    break;

            }

        }


        return soft;  //To change body of created methods use File | Settings | File Templates.
    }

    private void createSoftTable(List<Params> cols) {
        ArrayList<Pair> pairs = new ArrayList<Pair>();

        try {
            create(DBManager.getConnect(), "soft", true);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean parseBoolean(String s) {
        return s != null && (Boolean.parseBoolean(s) || s.equals("1"));
//        For  better understanding
//        if (s == null) {
//            return false;
//        }
//        if (Boolean.parseBoolean(s)) {
//            return true;
//        }
//        if (s.equals("1")) {
//            return true;
//        }
//        return false;

    }
}
