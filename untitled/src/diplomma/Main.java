package diplomma;

import diplomma.db.DBCreator;
import diplomma.db.DBManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    public static final String NAME         = "name";
    public static final String PRICE        = "price";
    public static final String ANTIVIRUS    = "antivirus";
    public static final String NSK          = "NSK";
    public static final String NSD          = "NSD";
    public static final String MONITORING   = "monitoring";
    public static final String SPYWARE      = "spyware";
    public static final String UPGRADE      = "upgrade";
    public static final String COMFORT      = "comfort";
    public static final String WINDOWS      = "Windows";
    public static final String LINUX        = "Linux";
    public static final String EFFECTIVE    = "effective";

    public static final String FEATURES = "features";
    public static List<String> fields = new ArrayList<String>() {{
        add(NAME      );
        add(PRICE     );
        add(ANTIVIRUS );
        add(NSK       );
        add(NSD       );
        add(MONITORING);
        add(SPYWARE   );
        add(UPGRADE   );
        add(COMFORT   );
        add(WINDOWS   );
        add(LINUX     );
        add(EFFECTIVE );

    }};

    public static List<String> getFields() {
        return fields;
    }

    public static void main(String[] args) throws SQLException, IOException {
        if (args.length > 0) {
            String arg = args[0];
            Logger.DEBUG = arg.equals("true");
        }
        DBCreator creator = new DBCreator();
        creator.create(DBManager.getConnect(),"soft",true);

        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../sample.fxml"));
        stage.setTitle("Diploma");
        stage.setScene(new Scene(root, 700, 500));
        stage.show();
    }
}
